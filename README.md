A handful generic list C library

*if you have any request regarding the project please contact the author at <sangsoic@protonmail.com>*

## About

* This C list library is [generic](https://en.wikipedia.org/wiki/Generic_programming).
* Documentation is available in the Doc directory.
* You can also read the next section of this page in order to understand basic usage.
* If you would like to know more about the general concept of list [read this document](https://en.wikipedia.org/wiki/Linked_list).
* Feel free to submit changes.

## Usage

There is two way to access routines. The easiest is using wrapper routine with macros and the more advanced way is to call the original generic version of the routine. The following sections cover each techniques in depth.

List typed objects contains 2 fields:

* head which corresponds to the head sentinel.
* tail which corresponds to the tail sentinel.
* size which is the current number of element contained inside the list.

### Wrapper routine

This method allows you to call macros to define specially typed structures and routines.

#### Macro syntax

```
LIST_[ROUTINE IDENTIFIER](<TYPE>, <NAME>)
```

Where *[ROUTINE IDENTIFIER]* is the upper case routine name, if no routine name specified then it declares the specially typed vector structure, *\<TYPE\>* the vector type, *\<NAME\>* an identifier.  
To learn more about each macro read the [documentation][documentation].

#### Example

```C
#include <stdio.h>
#include "list.h"

LIST(int, Int)
LIST_MALLOC(int, Int)
LIST_PUSH_FRONT(int, Int)
LIST_PUSH_BACK(int, Int)
LIST_AT_HEAD(int, Int)
LISTELEMENT_ISTAILSENTINEL(int, Int)
LISTELEMENT_GET(int, Int)
LIST_FREE(int, Int)

int main (void)
{
	ListInt * list;
	ListElementInt * current;
	size_t i;
	list = ListInt_malloc();
	for (i = 0; i < 3; i++) {
		ListInt_push_back(list, i);
	}
	for (i = 0; i < 3; i++) {
		ListInt_push_front(list, i);
	}
	current = ListInt_at_head(list);
	while (! ListElementInt_istailsentinel(current)) {
		printf("%d\n", ListElementInt_get(current));
		current = current->next;
	}
	ListInt_free(&list);
	return 0;
}
```

### Generic routine

This method allows you to call generic routine directly, however you will need to handle casting manually.  
To learn more about each generic routine you can read the [documentation][documentation]

#### Exemple

```C
#include <stdio.h>
#include "list.h"

int main (void)
{
	int tab[3] = {0, 1, 2};
	List * list;
	ListElement * current;
	size_t i;
	list = List_malloc();
	for (i = 0; i < 3; i++) {
		List_push_back(list, tab + i, sizeof(int));
	}
	for (i = 0; i < 3; i++) {
		List_push_front(list, tab + i, sizeof(int));
	}
	current = List_at_head(list);
	while (! ListElement_istailsentinel(current)) {
		printf("%d\n", *((int *)current->value));
		current = current->next;
	}
	List_free(&list);
	return 0;
}
```

[documentation]: Doc/html/files.html
