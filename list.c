/**
 * \file list.c
 * \brief A Handy generic list library
 * \author sangsoic <sangsoic@protonmail.com>
 * \version 0.1
 * \date 2020-04-21 Tue 06:06 PM
 * This file contains all routines that manages \a List object type.
 * \copyright
 *
 * Copyright 2020 Sangsoic library author
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *             http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "list.h"

static ListElement * ListElement_mallocinit(const void * const value, const size_t offset)
{
	ListElement * element;
	element = malloc(sizeof(ListElement));
	if (element == NULL) {
		fprintf(stderr, "error: %s.", strerror(errno));
		exit(EXIT_FAILURE);
	}
	if (offset == 0) {
		fprintf(stderr, "error: can't allocate cause offset is equal to 0.\n");
		exit(EXIT_FAILURE);
	}
	element->value = malloc(offset);
	if (element->value == NULL) {
		fprintf(stderr, "error: %s.", strerror(errno));
		exit(EXIT_FAILURE);
	}
	memcpy(element->value, value, offset);
	return element;
}

static void ListSentinel_allocinit(List * const list)
{
	ListElement * sentinelHead, * sentinelTail;
	sentinelHead = malloc(sizeof(ListElement));
	if (sentinelHead == NULL) {
		fprintf(stderr, "error: %s.\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	sentinelTail = malloc(sizeof(ListElement));
	if (sentinelTail == NULL) {
		fprintf(stderr, "error: %s.\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	sentinelHead->previous = NULL;
	sentinelHead->next = sentinelTail;
	sentinelHead->value = NULL;
	sentinelTail->previous = sentinelHead;
	sentinelTail->next = NULL;
	sentinelTail->value = NULL;
	list->head = sentinelHead;
	list->tail = sentinelTail;
}

List * List_malloc(void)
{
	List * list;
	list = malloc(sizeof(List));
	if (list == NULL) {
		fprintf(stderr, "error: %s.\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	ListSentinel_allocinit(list);
	list->size = 0;
	return list;
}

bool List_isempty(const List * const list)
{
	return list->size == 0;
}

bool ListElement_isheadsentinel(const ListElement * const element)
{
	return element->previous == NULL;
}

bool ListElement_istailsentinel(const ListElement * const element)
{
	return element->next == NULL;
}

ListElement * List_at_head(const List * const list)
{
	return list->head->next;
}

ListElement * List_at_tail(const List * const list)
{
	return list->tail->previous;
}

void List_swap(List * const list0, List * const list1)
{
	List tmp;
	tmp = *list0;
	*list0 = *list1;
	*list1 = tmp;
}

void List_push_front(List * const list, const void * const value, const size_t offset)
{
	ListElement * element;
	element = ListElement_mallocinit(value, offset);
	element->previous = list->head;
	element->next = list->head->next;
	list->head->next->previous = element;
	list->head->next = element;
	list->size++;
}

void List_push_back(List * const list, const void * const value, const size_t offset)
{
	ListElement * element;
	element = ListElement_mallocinit(value, offset);
	element->previous = list->tail->previous;
	element->next = list->tail;
	list->tail->previous->next = element;
	list->tail->previous = element;
	list->size++;
}

ListElement * List_insert_from_head(List * const list, const void * const value, const size_t index, const size_t offset)
{
	ListElement * dest, * tmp;
	size_t i;
	if (index >= list->size) {
		fprintf(stderr, "error: cannot insert because index out of range.\n");
		exit(EXIT_FAILURE);
	}
	dest = list->head;
	for (i = 0; i < index; i++) {
		dest = dest->next;
	}
	tmp = list->head;
	list->head = dest;
	List_push_front(list, value, offset);
	list->head = tmp;
	return dest->next;
}

ListElement * List_insert_from_tail(List * const list, const void * const value, const size_t index, const size_t offset)
{
	ListElement * dest, * tmp;
	size_t i;
	if (index >= list->size) {
		fprintf(stderr, "error: cannot insert because index out of range.\n");
		exit(EXIT_FAILURE);
	}
	dest = list->tail;
	for (i = 0; i < index; i++) {
		dest = dest->previous;
	}
	tmp = list->tail;
	list->tail = dest;
	List_push_back(list, value, offset);
	list->tail = tmp;
	return dest->previous;
}

void List_pop_front(List * const list)
{
	ListElement * tmp;
	if (List_isempty(list)) {
		fprintf(stderr, "error, cannot pop an empty list.\n");
		exit(EXIT_FAILURE);
	}
	tmp = list->head->next;
	free(tmp->value);
	list->head->next = tmp->next;
	tmp->next->previous = list->head;
	free(tmp);
	list->size--;
}

void List_pop_back(List * const list)
{
	ListElement * tmp;
	if (List_isempty(list)) {
		fprintf(stderr, "error, cannot pop an empty list.\n");
		exit(EXIT_FAILURE);
	}
	tmp = list->tail->previous;
	free(tmp->value);
	list->tail->previous = tmp->previous;
	tmp->previous->next = list->tail;
	free(tmp);
	list->size--;
}

void List_remove_from_here(List * const list, ListElement * const here, const size_t index)
{
	ListElement * dest, * tmp;
	size_t i;
	dest = here->previous;
	for (i = 0; i < index; i++) {
		dest = dest->next;
	}
	tmp = list->head;
	list->head = dest;
	List_pop_front(list);
	list->head = tmp;
}

void List_remove_this(List * const list, ListElement * const element)
{
	ListElement * tmp;
	tmp = list->head;
	list->head = element->previous;
	List_pop_front(list);
	list->head = tmp;
}

void List_remove_from_head(List * const list, const size_t index)
{
	ListElement * dest, * tmp;
	size_t i;
	if (index >= list->size) {
		fprintf(stderr, "error: cannot insert because index out of range.\n");
		exit(EXIT_FAILURE);
	}
	dest = list->head;
	for (i = 0; i < index; i++) {
		dest = dest->next;
	}
	tmp = list->head;
	list->head = dest;
	List_pop_front(list);
	list->head = tmp;
}

void List_remove_from_tail(List * const list, const size_t index)
{
	ListElement * dest, * tmp;
	size_t i;
	if (index >= list->size) {
		fprintf(stderr, "error: cannot insert because index out of range.\n");
		exit(EXIT_FAILURE);
	}
	dest = list->tail;
	for (i = 0; i < index; i++) {
		dest = dest->previous;
	}
	tmp = list->tail;
	list->tail = dest;
	List_pop_back(list);
	list->tail = tmp;
}

ListElement * List_whereis(const List * const list, const void * const value, const size_t offset)
{
	ListElement * current;
	current = list->head->next;
	while ((! ListElement_istailsentinel(current)) && (memcmp(value, current->value, offset) != 0)) {
		current = current->next;
	}
	return current;
}

size_t List_count(const List * const list, const void * const value, const size_t offset)
{
	ListElement * current;
	size_t count;
	current = list->head->next;
	count = 0;
	while (! ListElement_istailsentinel(current)) {
		if (memcmp(value, current->value, offset) == 0) {
			count++;
		}
		current = current->next;
	}
	return count;
}

size_t List_indexof(const List * const list, const void * const value, const size_t offset)
{
	ListElement * current;
	size_t index;
	current = list->head->next;
	index = 0;
	while ((! ListElement_istailsentinel(current)) && (memcmp(value, current->value, offset) != 0)) {
		current = current->next;
		index++;
	}
	return index;
}

List * List_cat(const List * const list0, const List * const list1, const size_t offset)
{
	List * cat;
	ListElement * current;
	cat = List_malloc();
	current = list0->head->next;
	while (! ListElement_istailsentinel(current)) {
		List_push_back(cat, current->value, offset);
		current = current->next;
	}
	current = list1->head->next;
	while (! ListElement_istailsentinel(current)) {
		List_push_back(cat, current->value, offset);
		current = current->next;
	}
	return cat;
}

List * List_copy(const List * const list, const size_t offset)
{
	List * copy;
	ListElement * current;
	copy = List_malloc();
	current = list->head->next;
	while (! ListElement_istailsentinel(current)) {
		List_push_back(copy, current->value, offset);
		current = current->next;
	}
	return copy;
}

void List_free(List * * const list)
{
	while (! List_isempty(*list)) {
		List_pop_front(*list);
	}
	free((*list)->head);
	free((*list)->tail);
	free(*list);
	*list = NULL;
}

void List_empty(List * const list)
{
	while (! List_isempty(list)) {
		List_pop_front(list);
	}
}

void List_bind(List * const list0, List * * const list1)
{
	list0->tail->previous->next = (*list1)->head->next;
	(*list1)->head->next->previous = list0->tail->previous;
	list0->tail->previous = (*list1)->tail->previous;
	(*list1)->tail->previous->next = list0->tail;
	list0->size += (*list1)->size;
	(*list1)->size = 0;
	List_free(list1);
}
